class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :ProductID
      t.string :Category
      t.string :ProductName
      t.integer :Price

      t.timestamps null: false
    end
  end
end
