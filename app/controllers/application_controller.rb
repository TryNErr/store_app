class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
    def import
      Product.import(params[:file])
      flash[:success] = "Import Complete"
      redirect_to '/products'
    end
    def start
    end
    def bycat
    end

end
