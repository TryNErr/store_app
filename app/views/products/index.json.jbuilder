json.array!(@products) do |product|
  json.extract! product, :id, :ProductID, :Category, :ProductName, :Price
  json.url product_url(product, format: :json)
end
