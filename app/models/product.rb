class Product < ActiveRecord::Base
  def self.import(file)
    # Delete all existing Products
    ActiveRecord::Base.connection.execute("Delete from products;")
    
    # Reset the sequence
    ActiveRecord::Base.connection.execute("delete from sqlite_sequence where name='products';")
     
    CSV.foreach(file.path, headers: true) do |row|
      rowhash = row.to_hash
      rowhash['ProductName'] = rowhash.delete('Product Name')
      Product.create! rowhash
    end
  end
end
